global with sharing class ObjectFieldsMetricStructure implements Comparable {
    @AuraEnabled public String objectLabel;
    @AuraEnabled public String objectName;
    @AuraEnabled public Integer numberOfAllFields;
    @AuraEnabled public Integer numberOfStandardFields;
    @AuraEnabled public Integer numberOfCustomFields;
    @AuraEnabled public Integer numberOfPackagedFields;
    @AuraEnabled public Boolean hasCustomFields;
    public ObjectFieldsMetricStructure(String objectLabel, String objectName, Integer numberOfStandardFields, Integer numberOfCustomFields, Integer numberOfPackagedFields) {
        this.objectLabel = objectLabel;
        this.objectName = objectName;
        this.numberOfStandardFields = numberOfStandardFields;
        this.numberOfCustomFields = numberOfCustomFields;
        this.numberOfPackagedFields = numberOfPackagedFields;
        this.numberOfAllFields = numberOfStandardFields + numberOfCustomFields + numberOfPackagedFields;
        this.hasCustomFields = (numberOfCustomFields > 0 || numberOfPackagedFields > 0) ? true : false;
    }
    
    global Integer compareTo(Object compareTo) {
        ObjectFieldsMetricStructure compareToObject = (ObjectFieldsMetricStructure) compareTo;
        
        Integer returnValue = 0;

        if (objectLabel > compareToObject.objectLabel) {
            returnValue = 1;
        } else if (objectLabel < compareToObject.objectLabel) {
            returnValue = -1;
        }
        return returnValue;       
    }
}