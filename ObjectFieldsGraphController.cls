public with sharing class ObjectFieldsGraphController {
    public static Set<String> skippedObjects {
        get {
            if (skippedObjects == null) {
                skippedObjects = new Set<String>();
                skippedObjects.add('AuthorizationForm');
                skippedObjects.add('AuthorizationFormConsent');
                skippedObjects.add('AuthorizationFormDataUse');
                skippedObjects.add('AuthorizationFormText');
                skippedObjects.add('UserAppMenuCustomization');
                skippedObjects.add('UserProvisioningRequest');
                skippedObjects.add('UserProvisioningLog');
                skippedObjects.add('UserProvMockTarget');
                skippedObjects.add('UserProvAccount');
                skippedObjects.add('UserProvAccountStaging');
            }
            return skippedObjects;
        }
        set;
    }

    @AuraEnabled
    public static List<ObjectFieldsMetricStructure> getFieldsMetadata(){

        List<ObjectFieldsMetricStructure> objectFieldsMetrics = new List<ObjectFieldsMetricStructure>();
        Map<String, Schema.SObjectType> globalDescribe = Schema.getGlobalDescribe();
        String labels = '';

        for (Schema.SObjectType objectType : globalDescribe.values()) {    
            Schema.DescribeSObjectResult objectResult = objectType.getDescribe();
            Integer numberOfStandardFields = 0;
            Integer numberOfCustomFields = 0;
            Integer numberOfPackagedFields = 0;
            if (isValidObject(objectResult)) {
                for (Schema.SObjectField objectField : objectResult.fields.getMap().values()) {
                    Schema.DescribeFieldResult fieldResult = objectField.getDescribe();
                    if (fieldResult.isCustom()) {
                        //Check if its a Managed Package field
                        if (fieldResult.getName().removeEnd('__c').contains('__')) {
                            numberOfPackagedFields++;
                        } else {
                            numberOfCustomFields++;
                        }
                    } else {
                        numberOfStandardFields++;
                    }
                }
                objectFieldsMetrics.add(new ObjectFieldsMetricStructure(
                    objectResult.getLabel(),
                    objectResult.getName(),
                    numberOfStandardFields,
                    numberOfCustomFields,
                    numberOfPackagedFields
                ));
            }   
        }

        objectFieldsMetrics.sort();
        return objectFieldsMetrics;
    }

    private static Boolean isValidObject(Schema.DescribeSObjectResult objectResult) {
        Boolean isValid = true;
        if (!objectResult.isCreateable()) {
            isValid = false;
        }
        if (!objectResult.isUpdateable()) {
            isValid = false;
        }
        if (objectResult.getName().endsWith('Share')) {
            isValid = false;
        }
        if (objectResult.getName().endsWith('OwnerSharingRule')) {
            isValid = false;
        } 
        if (objectResult.getName().endsWith('History')) {
            isValid = false;
        }
        if (objectResult.getName().endsWith('Feed')) {
            isValid = false;
        }
        if (skippedObjects.contains(objectResult.getName())) {
            isValid = false;
        }
        return isValid;
    }
}