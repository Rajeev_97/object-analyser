import { LightningElement, track, api} from 'lwc';
import chartjs from '@salesforce/resourceUrl/Chartjs';
import getFieldsMetadata from '@salesforce/apex/ObjectFieldsGraphController.getFieldsMetadata'
import { loadScript } from 'lightning/platformResourceLoader';

export default class objectFieldsGraph extends LightningElement {

    @api selectedObjectNames = ['Account', 'Contact', 'Opportunity', 'Case', 'Product2'];

    @track hasRendered = false;
    @track chartjs = chartjs;
    @track allObjectsFields;
    @track barChartData = {};
    @track allObjectsNames = [];
    @track isLoading = true;
    barChart;
    error;

    renderedCallback() {
        if (this.hasRendered) {
            return;
        }
        this.hasRendered = true;
        Promise.all([
            loadScript(this, this.chartjs)
        ]).then(() => {
            this.generateBarChart();
        })
        .catch(error => {
            this.error = error;
            console.log(' Error Occured ', error);
        });
    }

    errorCallback(error, stack) {
        this.error = error;
        console.log(' this.error ', this.error);
    }

    generateBarChart() {
        getFieldsMetadata().then(result => { 
            this.allObjectsFields = result;
            this.prepareListOfObjects();
            this.prepareGraphData();
        })
        .catch(error => {
            this.error = error;
        });
    }

    prepareListOfObjects() {
        for (var index in this.allObjectsFields) {
            var objectFieldMetric = this.allObjectsFields[index];
            this.allObjectsNames.push({
                label: objectFieldMetric.objectLabel + ' (' + objectFieldMetric.numberOfAllFields + ')', 
                value: objectFieldMetric.objectName
            });
        }
    }

    prepareGraphData() {
        if (this.barChart) {
            this.barChart.destroy();
        }
        var objectLabels = [];
        var standardFieldsCount = [];
        var customFieldsCount = [];
        var packagedFieldsCount = [];
        for (var index in this.allObjectsFields) {
            var objectFieldMetric = this.allObjectsFields[index];

            if (this.selectedObjectNames.indexOf(objectFieldMetric.objectName) > -1) {
                objectLabels.push(objectFieldMetric.objectLabel);
                standardFieldsCount.push(objectFieldMetric.numberOfStandardFields);
                customFieldsCount.push(objectFieldMetric.numberOfCustomFields);
                packagedFieldsCount.push(objectFieldMetric.numberOfPackagedFields);
            }
        }

        this.barChartData = {
            labels : objectLabels,
            datasets: [{
                label: 'Standard Fields',
                backgroundColor: 'rgba(126, 222, 31, 0.5)',
                data: standardFieldsCount
            },
            {
                label: 'Custom Fields',
                backgroundColor: 'rgba(237, 108, 57, 0.5)',
                data: customFieldsCount
            },
            {
                label: 'Packaged Fields',
                backgroundColor: 'rgba(232, 232, 16, 0.5)',
                data: packagedFieldsCount
            }]
        };

        var dataSet = {
            type: 'horizontalBar',
            data: this.barChartData,
            options: {
                responsive: true,
                title: {
                    display: true,
                    text: 'Fields on Objects'
                },
                tooltips: {
                    mode: 'index',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        type: 'linear',
                        display: true,
                        position: 'left',
                        stacked: true
                    }],
                    yAxes: [{
                        stacked: true
                    }],
                }
            }
        }

        this.isLoading = false;

        const ctx = this.template
            .querySelector('canvas.barChart')
            .getContext('2d');

        this.barChart = new window.Chart(ctx, dataSet);

    }

    handleChange(event) {
        this.selectedObjectNames = event.detail.value;
        this.prepareGraphData();
    }

    downloadAsCsvFile(event) {
        let downloadSelected = false;
        if (event.target.label == 'Download Selected') {
            downloadSelected = true;
        }

        let fileName = downloadSelected ? 'ObjectFieldAnalyserSelectedObjects.csv' : 'ObjectFieldAnalyserAllObjects.csv'
        let rowEnd = '\n';
        let separator = ',';
        let csvString = '';

        csvString += 'Object Label' + separator 
                    + 'Object Name' + separator 
                    + 'Number of Standard Fields' + separator 
                    + 'Number of Custom Fields' + separator 
                    + 'Number of Packaged Fields' + rowEnd;

        for (var index in this.allObjectsFields) {
            var objectFieldMetric = this.allObjectsFields[index];
            if (!downloadSelected || (downloadSelected && this.selectedObjectNames.indexOf(objectFieldMetric.objectName) > -1)) {
                csvString += objectFieldMetric.objectLabel + separator;
                csvString += objectFieldMetric.objectName + separator;
                csvString += objectFieldMetric.numberOfStandardFields + separator;
                csvString += objectFieldMetric.numberOfCustomFields + separator;
                csvString += objectFieldMetric.numberOfPackagedFields + rowEnd;
            }
        }

        let downloadElement = document.createElement('a');
        downloadElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csvString);
        downloadElement.target = '_blank';
        downloadElement.download = fileName;
        document.body.appendChild(downloadElement);
        downloadElement.click(); 
    }

}